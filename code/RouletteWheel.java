import java.util.Random;

public class RouletteWheel {
    private Random o;
    private int number;

    public RouletteWheel(){
        this.o      = new Random();
        this.number = 0;
    }

    public void spin(){
        this.number = o.nextInt(37);
    }
    public int getValue(){
        return this.number;
    }
}
