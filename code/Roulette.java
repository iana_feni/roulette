
import java.util.Scanner;
public class Roulette {
    public static void main(String[] args){
        RouletteWheel x = new RouletteWheel();
        Scanner scan = new Scanner(System.in); 
      
        System.out.println("HEY! Would you like to bet on a number ?");
        String betChoice = scan.next();

        if(betChoice.equals("yes")){
            System.out.println("What number would you like to bet on ? Must be between 1-36!"); 
            int betNumber   = scan.nextInt();
            int betMoney    = 0;
            boolean isValid = false; 	

            while(!(isValid))
				{
					try{
							System.out.println("How much would you like to bet on ? Notice, you only have 1000$! So we expect to se a number between 0 and 1000!");
							betMoney = scan.nextInt();
							if(betMoney<0 || betMoney>1000)
								{
									throw new IllegalArgumentException();
								}
							else
								{
									isValid = true;
								}
						}
					catch(IllegalArgumentException e)
						{
							System.out.println("You haven't provided a legal number!! Please re-enter a number between 0$ and 1000$!");
							System.out.println();
						}
				}
        
        
            x.spin();
            int chosenNumber = x.getValue();
            if(chosenNumber==betNumber)
            {
                System.out.println("Congrats! You just won "+betMoney*35+"$!!");
            }
            else
            {
                System.out.println("You just lost "+betMoney+"$! Sad life :( ");
            }
        }
    }
}
